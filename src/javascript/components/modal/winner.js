import { showModal } from './modal'

export function showWinnerModal(fighter) {
  showModal({ 
    title: `${fighter.name} won!`, 
    bodyElement: 'Do you want to play again?',
    onClose = () => {
      window.location.reload();
    },
  }); 
}
