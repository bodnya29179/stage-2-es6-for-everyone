import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // Create fighter image element
  const imageElement = createFighterImage(fighter);
  
  // Create fighter name element
  const nameElement = createElement({
    tagName: 'h1',
    className: 'name',
  });

  nameElement.innerText = fighter.name.toUpperCase();

  // Create label for health progress bar
  const healthLabel = createElement({
    tagName: 'label',
    for: `health${fighter._id}`,
  });
  
  healthLabel.innerText = 'Health';

  // Create fighter progress bar of health points
  const healthElement = createElement({
    tagName: 'progress',
    id: `health${fighter._id}`,
    className: 'health',
  });

  healthElement.value = fighter.health;
  healthElement.max = 100;

  // Create label for attack progress bar
  const attackLabel = createElement({
    tagName: 'label',
    for: `attack${fighter._id}`,
  });
  
  attackLabel.innerText = 'Attack';

  // Create fighter progress bar of attack points
  const attackElement = createElement({
    tagName: 'progress',
    id: `attack${fighter._id}`,
    className: 'attack',
  });

  attackElement.value = fighter.attack;
  attackElement.max = 5;

  // Create label for defense progress bar
  const defenseLabel = createElement({
    tagName: 'label',
    for: `defense${fighter._id}`,
  });
  
  defenseLabel.innerText = 'Defense';

  // Create fighter progress bar of defense points
  const defenseElement = createElement({
    tagName: 'progress',
    id: `defense${fighter._id}`,
    className: 'defense',
  });
 
  defenseElement.value = fighter.defense;
  defenseElement.max = 5;

  // Add all elements to HTML-page
  fighterElement.append(
    imageElement, 
    nameElement, 
    healthLabel, 
    healthElement, 
    attackLabel, 
    attackElement, 
    defenseLabel, 
    defenseElement
  );

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
